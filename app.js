const lugar = require('./lugar/lugar');
const clima = require('./clima/clima');

const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'direccion de la ciudad para obtener el clima',
        demand: true
    }
}).argv;

/*lugar.getLugarLatLng(argv.direccion)
    .then((result) => {
        console.log(result);
    }).catch((err) => {
        console.log(err);
    });

clima.getClima(41.389999, 2.160000)
    .then((result) => {
        console.log(result);
    }).catch((err) => {
        console.log(err);
    });*/

const getInfo = async(direccion) => {
    try {
        const cordenadas = await lugar.getLugarLatLng(direccion);
        const temperatura = await clima.getClima(cordenadas.lat, cordenadas.lng);
        return `el clima de ${ cordenadas.location } es ${ temperatura }`;
    } catch (error) {
        return `no se pudo determinar el clima de ${ direccion }`
    }


}

getInfo(argv.direccion)
    .then((result) => {
        console.log(result);
    }).catch((err) => {
        console.log(err);
    });;
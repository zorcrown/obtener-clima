const axios = require('axios');

const getLugarLatLng = async(direccion) => {
    const encodeURL = encodeURI(direccion);

    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${ encodeURL }`,
        headers: { 'X-RapidAPI-Key': 'a3250eb935mshb815caa5fe36593p1f84f3jsn8c63bc7fe49b' }
    })

    const respuesta = await instance.get();

    if (respuesta.data.Results.length === 0) {
        throw new Error(`no hay resultados para ${ direccion}`);
    }

    const data = respuesta.data.Results[0];
    const location = data.name;
    const lat = data.lat;
    const lng = data.lon;

    return {
        location,
        lat,
        lng
    }
}


module.exports = {
    getLugarLatLng
}